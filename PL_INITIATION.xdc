## LEDs
set_property PACKAGE_PIN V17 [get_ports {D1}]
set_property IOSTANDARD LVCMOS33 [get_ports {D1}]
set_property PACKAGE_PIN V16 [get_ports {D2}]
set_property IOSTANDARD LVCMOS33 [get_ports {D2}]
set_property PACKAGE_PIN W16 [get_ports {D3} ]
set_property IOSTANDARD LVCMOS33 [get_ports {D3}]

set_property PACKAGE_PIN U16 [get_ports Y1]
set_property IOSTANDARD LVCMOS33 [get_ports Y1]
set_property PACKAGE_PIN E19 [get_ports Y2]
set_property IOSTANDARD LVCMOS33 [get_ports Y2]
set_property PACKAGE_PIN U19 [get_ports Y3]
set_property IOSTANDARD LVCMOS33 [get_ports Y3]
set_property PACKAGE_PIN V19 [get_ports Y4]
set_property IOSTANDARD LVCMOS33 [get_ports Y4]
set_property PACKAGE_PIN W18 [get_ports Y5]
set_property IOSTANDARD LVCMOS33 [get_ports Y5]

## Configuration options, can be used for all designs
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]


